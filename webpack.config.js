const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
    entry: './src/index.js', 
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
      },
    devServer: {
        // contentBase: path.join(__dirname, '/'),
        contentBase: './build',
        compress: true,
        port: 9000
    },
    module: {
        rules: [
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader', 'eslint-loader'],            
        },
        {
            test: /\.less$/,
            use: [
              'style-loader',
              'css-loader',
              'less-loader',
            ],
        }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
          template: path.resolve('./index.html'),
        }),
    ],
    devtool: 'inline-source-map',
}
module.exports = config;